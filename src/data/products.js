export default [
  {
    id: 1,
    categoryId: 5,
    title: 'Наушники',
    price: '7000',
    image: '/img/airpods.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 2,
    categoryId: 4,
    title: 'Motorola MBP16 Радионяня',
    price: '3690',
    image: '/img/radio.jpg',
    colorsItem: ['#73B6EA', '#FF6B00', '#939393']
  },
  {
    id: 3,
    categoryId: 3,
    title: 'Электроборд',
    price: '12900',
    image: '/img/board.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 4,
    categoryId: 2,
    title: 'Мобильник',
    price: '22990',
    image: '/img/phone.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 5,
    categoryId: 2,
    title: 'Смартфон',
    price: '12500',
    image: '/img/phone.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 6,
    categoryId: 4,
    title: 'Р16 Радионяня',
    price: '1260',
    image: '/img/radio.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 7,
    categoryId: 4,
    title: 'Радионяня Mercury',
    price: '32690',
    image: '/img/radio.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 8,
    categoryId: 3,
    title: 'Скутер',
    price: '36900',
    image: '/img/wheels.jpg',
    colorsItem: ['#FF6B00', '#FFBE15', '#939393']
  },
  {
    id: 9,
    categoryId: 3,
    title: 'Самокат',
    price: '36490',
    image: '/img/scooter.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  },
  {
    id: 10,
    categoryId: 4,
    title: 'Радионяня SpaceX',
    price: '1690',
    image: '/img/radio.jpg',
    colorsItem: ['#73B6EA', '#8BE000', '#939393']
  },
  {
    id: 11,colorId: 1,
    categoryId: 1,
    title: 'Зубная щетка',
    price: '3690',
    image: '/img/toothbrush.jpg',
    colorsItem: ['#73B6EA', '#FFBE15', '#939393']
  }

]
