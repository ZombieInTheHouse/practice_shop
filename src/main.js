import Vue from 'vue';
import App from './App.vue';
import {popup} from './utils.js';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');

// popup();
